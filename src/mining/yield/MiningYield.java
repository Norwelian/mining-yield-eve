/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package mining.yield;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.swing.*;
/**
 *
 * @author Norwelian
 */
public class MiningYield extends javax.swing.JFrame {

    double oreWeight[] = {0.1, 0.15, 0.3, 0.35, 0.6, 1.2, 2.0, 3.0, 3.0, 5.0, 8.0, 16.0, 16.0, 16.0, 16.0, 40.0}; 
    
    double oreHangerInt = 0.0;
    double shipHangerInt = 0.0;
    double yieldInt = 0.0;
    double cycleInt = 0.0;
    double asteroidCapInt = 0.0;
    BigDecimal orePerSec = BigDecimal.ZERO;
    BigDecimal m3PerSec = BigDecimal.ZERO;
    BigDecimal totalSum = BigDecimal.ZERO;
    boolean warnFull = false;
    
    Timer timer;
    Timer timer1;
    
    String savingDir = "save";
    
    /**
     * Creates new form MiningYield
     */
    public MiningYield() {
        initComponents();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        yield = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cycle = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        ore = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        oreHanger = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        shipHanger = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        submit = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        asteroidCap = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        orePerCycle = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        totalTimeToWarn = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        warnDepleted = new javax.swing.JCheckBox();
        warningReason = new javax.swing.JLabel();
        oreHangerActual = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        shipHangerActual = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        save = new javax.swing.JButton();
        load = new javax.swing.JButton();
        saveSelector = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        timeToWarn = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        cancel = new javax.swing.JButton();
        reset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mining timer");
        setFont(new java.awt.Font("Thames", 0, 12)); // NOI18N
        setPreferredSize(new java.awt.Dimension(390, 480));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel1.setText("Mining timer");

        jLabel2.setText("Yield:");

        jLabel3.setText("m^3 / cycle");

        jLabel4.setText("Cycle:");

        jLabel5.setText("s");

        ore.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Veldspar", "Scordite", "Pyroxeres", "Plagioclase", "Omber", "Kernite", "Jaspet", "Hemorphite", "Hedbergite", "Gneiss", "Dark Ochre", "Spodumain", "Crokite", "Bistot", "Arkonor", "Mercoxit" }));

        jLabel6.setText("Ore:");

        jLabel7.setText("Ore Hanger:");

        jLabel8.setText("m^3");

        jLabel9.setText("Ship Hanger:");

        jLabel10.setText("m^3");

        submit.setText("Submit");
        submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitActionPerformed(evt);
            }
        });

        jLabel11.setText("Asteroid cap:");

        jLabel12.setText("m^3");

        jLabel13.setText("Ores per sec:");

        orePerCycle.setText("0");

        jLabel15.setText("Total time to warn:");

        totalTimeToWarn.setText("0");

        jLabel17.setText("s");

        warnDepleted.setSelected(true);
        warnDepleted.setText("Warn me");
        warnDepleted.setToolTipText("Warn me when the first of this things occur:\n The asteroid is depleted\n The hold is full.");

        warningReason.setText("Warning reason");

        oreHangerActual.setText("0");

        jLabel14.setText("-");

        jLabel16.setText("m^3");

        jLabel18.setText("-");

        shipHangerActual.setText("0");
        shipHangerActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipHangerActualActionPerformed(evt);
            }
        });

        jLabel19.setText("m^3");

        save.setText("Save");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        load.setText("Load");
        load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadActionPerformed(evt);
            }
        });

        saveSelector.setText("skiff");

        jLabel20.setText("Total time to warn:");

        timeToWarn.setText("0");

        jLabel21.setText("s");

        cancel.setText("Cancel");
        cancel.setEnabled(false);
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });

        reset.setText("Reset Holds");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel13))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(orePerCycle)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(totalTimeToWarn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(submit)
                                .addGap(49, 49, 49)
                                .addComponent(cancel)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(ore, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(oreHanger, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(warningReason, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cycle, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                                            .addComponent(yield, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                                            .addComponent(shipHanger, javax.swing.GroupLayout.Alignment.LEADING)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(asteroidCap, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(oreHangerActual, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel16))
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(saveSelector)
                                                .addComponent(save, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(load, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(reset, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel18)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(shipHangerActual, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel19)))))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(warnDepleted)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addGap(18, 18, 18)
                                .addComponent(timeToWarn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel21)))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(101, 101, 101))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(yield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cycle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(oreHanger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(oreHangerActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(shipHanger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(shipHangerActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18)
                        .addComponent(jLabel19)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(ore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(asteroidCap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(warnDepleted)
                    .addComponent(warningReason)
                    .addComponent(saveSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(orePerCycle)
                    .addComponent(save))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(totalTimeToWarn)
                    .addComponent(jLabel17)
                    .addComponent(load))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(timeToWarn)
                    .addComponent(jLabel21)
                    .addComponent(reset))
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(submit)
                    .addComponent(cancel))
                .addGap(42, 42, 42))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void submitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitActionPerformed
        try{
        
        double oreHangerActualInt;
        double shipHangerActualInt;
        if(oreHangerActual.getText().equals("")){
            oreHangerActualInt = 0;
        } else {
            oreHangerActualInt = Double.parseDouble(oreHangerActual.getText());
        }
        if(shipHangerActual.getText().equals("")){
            shipHangerActualInt = 0;
        } else {
            shipHangerActualInt = Double.parseDouble(shipHangerActual.getText());
        }
        
        oreHangerInt = Double.parseDouble(oreHanger.getText());
        shipHangerInt = Double.parseDouble(shipHanger.getText());
        yieldInt = Double.parseDouble(yield.getText());
        cycleInt = Double.parseDouble(cycle.getText());
        asteroidCapInt = Double.parseDouble(asteroidCap.getText());
        
        yieldInt = Math.floor(yieldInt/oreWeight[ore.getSelectedIndex()])*oreWeight[ore.getSelectedIndex()];

        double oreHangerOresInt = Math.floor(oreHangerActualInt / oreWeight[ore.getSelectedIndex()]);
        double shipHangerOresInt = Math.floor(shipHangerActualInt / oreWeight[ore.getSelectedIndex()]);
        double oreHangerTotalInt = Math.floor(oreHangerInt / oreWeight[ore.getSelectedIndex()]);
        double shipHangerTotalInt = Math.floor(shipHangerInt / oreWeight[ore.getSelectedIndex()]);
        
        double hangerInt = (oreHangerTotalInt - oreHangerOresInt) + (shipHangerTotalInt - shipHangerOresInt);
        
        m3PerSec = BigDecimal.valueOf(yieldInt).divide(BigDecimal.valueOf(cycleInt), 10000, RoundingMode.HALF_DOWN); //<<<<<----- THIS
        orePerSec = m3PerSec.divide(BigDecimal.valueOf(oreWeight[ore.getSelectedIndex()]), 10000, RoundingMode.HALF_DOWN);
        orePerCycle.setText(String.valueOf(Math.floor(orePerSec.doubleValue())));
        
        //showAlert("m3: " + m3PerSec + "\norePerSec: " + orePerSec + "\nyieldInt: " + yieldInt);
        
        double timeToFullInt = hangerInt / orePerSec.doubleValue();
        double timeToDepleteInt = asteroidCapInt / orePerSec.doubleValue();
        
        //showAlert("Ore total: " + oreHangerTotalInt + "\nOre actual: " + oreHangerOresInt + "\nTimeToFull: " + timeToFullInt);
        //showAlert("m3persec: " + m3PerSec + "\norePerSec: " + orePerSec + "\nHangerInt: " + hangerInt);
        
        String msg;
        int time;
        if(timeToFullInt <= timeToDepleteInt){
            time = (int)Math.floor(timeToFullInt * 1000);
            msg = "The hold is full.";
            warningReason.setText("Full hold");
            warnFull = true;
        } else {
            time = (int)Math.floor(timeToDepleteInt * 1000);
            msg = "The asteroid is depleted.";
            warningReason.setText("Depleted asteroid");
            warnFull = false;
        }
        totalTimeToWarn.setText(String.valueOf(Math.round(time/1000)));
        timeToWarn.setText(String.valueOf(Math.round(time/1000)));
        
        Process p = Runtime.getRuntime().exec("SendKey.exe \"EVE -\" F1");
        
        if(warnDepleted.isSelected()){
            submit.setEnabled(false);
            cancel.setEnabled(true);
            
            totalSum = BigDecimal.ZERO;
            
            timer1 = new Timer(1000, new ActionListener(){
                public void actionPerformed (ActionEvent e){
                    timeToWarn.setText(String.valueOf(Integer.parseInt(timeToWarn.getText())-1));
                    increaseOreTick();
                }
            });
            timer1.setRepeats(true);
            timer1.start();
            
            timer = new Timer(time, new ActionListener (){
                public void actionPerformed (ActionEvent e){
                    submit.setEnabled(true);
                    cancel.setEnabled(false);
                    timer1.stop();
                    lastTick();
                    showAlert(msg);
                }
            });
            timer.setRepeats(false);
            timer.start();
        }
        
        } catch(Exception e){
            showAlert(e.toString());
        }
    }//GEN-LAST:event_submitActionPerformed

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        saveFile(saveSelector.getText() + ".nigger");
    }//GEN-LAST:event_saveActionPerformed

    private void loadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadActionPerformed
        loadFile(saveSelector.getText() + ".nigger");
    }//GEN-LAST:event_loadActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        try{
            //Process p = Runtime.getRuntime().exec("SendKey.exe \"EVE -\" F1");
            if(timer.isRunning()){
                timer.stop();
            }
            if(timer1.isRunning()){
                timer1.stop();
            }
        } catch(Exception e){
            showAlert(e.toString());
        }
        
        cancel.setEnabled(false);
        submit.setEnabled(true);
        
    }//GEN-LAST:event_cancelActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
        resetHolds();
    }//GEN-LAST:event_resetActionPerformed

    private void shipHangerActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipHangerActualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_shipHangerActualActionPerformed

    
    private void showAlert(String msg){
         JOptionPane optionPane = new JOptionPane();
            Runnable SOUND = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
            JDialog dialog = optionPane.createDialog("Warning");

            optionPane.setMessage(msg);
            dialog.setAlwaysOnTop(true);
            SOUND.run();
            dialog.setVisible(true);
            System.out.println(msg);
    }
    
    private void increaseOreTick(){
        BigDecimal actualShip = BigDecimal.valueOf(Double.parseDouble(shipHangerActual.getText()));
        BigDecimal totalShip = BigDecimal.valueOf(Double.parseDouble(shipHanger.getText()));
        BigDecimal actualO = BigDecimal.valueOf(Double.parseDouble(oreHangerActual.getText()));
        
        if((actualShip.add(m3PerSec)).compareTo(totalShip) <= 0 ){
            shipHangerActual.setText(String.format("%.1f", (actualShip).add(m3PerSec)));
        } else {
            oreHangerActual.setText(String.format("%.1f", (actualO).add(m3PerSec)));
        }
        totalSum = totalSum.add(m3PerSec);
    }
    
    private void lastTick(){
        BigDecimal actualO = BigDecimal.valueOf(Double.parseDouble(oreHangerActual.getText()));
        if(warnFull)
            oreHangerActual.setText(String.format("%.1f", (actualO).add(BigDecimal.valueOf(oreHangerInt).subtract(totalSum))));
        else
            oreHangerActual.setText(String.format("%.1f", (actualO).add(BigDecimal.valueOf(asteroidCapInt).subtract(totalSum))));

    }
    
    private void saveFile(String name){
        try {
            getProps();
            String saving = yieldInt + "\n" + cycleInt + "\n" + oreHangerInt + "\n" + shipHangerInt + "\n" + ore.getSelectedIndex();
            File file = new File("save");
            if(!file.exists()){
                file.mkdir();
            }
            File file1 = new File(file, name);
            if(!file1.exists()){
                file1.createNewFile();
            }
            FileWriter fw = new FileWriter(file1.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(saving);
            bw.close();
            showAlert("Saved");

        } catch (Exception e) {
            showAlert(e.toString());
        }
    }
    
    private void loadFile(String name){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(savingDir + "/" + name));
            yield.setText(reader.readLine());
            cycle.setText(reader.readLine());
            oreHanger.setText(reader.readLine());
            shipHanger.setText(reader.readLine());
            ore.setSelectedIndex(Integer.parseInt(reader.readLine()));
            reader.close();
            showAlert("Loaded");
        } catch (Exception e) {
            showAlert(e.toString());
        }
    }
        
    private void getProps(){        
        yieldInt = Double.parseDouble(yield.getText());
        cycleInt = Double.parseDouble(cycle.getText());
        oreHangerInt = Double.parseDouble(oreHanger.getText());
        shipHangerInt = Double.parseDouble(shipHanger.getText());
    }
    
    private void resetHolds(){
        oreHangerActual.setText("0");
        shipHangerActual.setText("0");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MiningYield.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MiningYield.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MiningYield.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MiningYield.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MiningYield().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField asteroidCap;
    private javax.swing.JButton cancel;
    private javax.swing.JTextField cycle;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton load;
    private javax.swing.JComboBox ore;
    private javax.swing.JTextField oreHanger;
    private javax.swing.JTextField oreHangerActual;
    private javax.swing.JLabel orePerCycle;
    private javax.swing.JButton reset;
    private javax.swing.JButton save;
    private javax.swing.JTextField saveSelector;
    private javax.swing.JTextField shipHanger;
    private javax.swing.JTextField shipHangerActual;
    private javax.swing.JButton submit;
    private javax.swing.JLabel timeToWarn;
    private javax.swing.JLabel totalTimeToWarn;
    private javax.swing.JCheckBox warnDepleted;
    private javax.swing.JLabel warningReason;
    private javax.swing.JTextField yield;
    // End of variables declaration//GEN-END:variables
}
